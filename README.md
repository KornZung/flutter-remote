# flutter-remote

Setup remote development flutter

https://medium.com/flutter-community/how-to-dockerize-flutter-apps-f2e54d6ec43c

=== Server : Ubuntu ===
- Docker ต้องลงแบบตัวเต็ม
https://docs.docker.com/engine/install/ubuntu/

- กำหนดสิทธิ์ให้ docker
https://docs.docker.com/engine/install/linux-postinstall/




=== Local ===
- docker client ไม่ได้ ต้องลงเต็มๆ
https://master.dockerproject.com/
https://master.dockerproject.com/windows/x86_64/docker.zip

- VS Code
	- Preference : setting json
		"docker.host": "tcp://localhost:23750"
- SSH
	ssh -NL localhost:23750:/var/run/docker.sock a@192.168.182.134

- Folder
	- สร้างไฟล์ .devcontainer/devcontainer.json
	- สร้างไฟล์ Dockerfile

	- Run : Remote open file ไปที่ folder หลัก

- Issue
	- หา Path /tmp/vsch ไม่เจอ
		- ต้องลง docker full version ที่เครื่อง Client

	- Connect Android Device
		- ต้องลง adb หรือ android SDK ไว้ที่ Local


=============